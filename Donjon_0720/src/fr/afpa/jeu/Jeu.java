package fr.afpa.jeu;

import java.util.Scanner;

import fr.afpa.composant.*;
import fr.afpa.plateau.Donjon;

public class Jeu {
	private Donjon donjon;
	private Joueur joueur;
	private ItemsMonsterCheptel itemsMonsterCheptel;
	private int end;
	
	public Jeu() {
		donjon = new Donjon(6,3);
		joueur = new Joueur(0,0);
		itemsMonsterCheptel = new ItemsMonsterCheptel(8, 5, 6, 3);
		end = donjon.getEnd();
	}
	
//------------------------------------------------------------AFFICHAGE----------------------------------------------------------------------------------------------------------	
	// Affichage de la map avec le joueur
	public void displayDonjonGamer() {
		for (int i = 0; i < donjon.getY(); i++) {
			// draw the north edge
			for (int j = 0; j < donjon.getX(); j++) {
				System.out.print(donjon.getDonjon()[j][i].getDoorN());
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < donjon.getX(); j++) {
				if(j == 0 && i == 0) {
					System.out.print(donjon.getDonjon()[j][i].getDoorW() );
				} else if(donjon.getDonjon()[j][i].getId() == donjon.getEnd()){
					if((donjon.getDonjon()[j][i].getBit() & 8) == 0) {
						System.out.print("|>< " );
					} else {
						System.out.print(" >< " );
					}
				}else {
					System.out.print(donjon.getDonjon()[j][i].getDoorW());
				}
			}
			System.out.println("|");
		}
		// draw the bottom line
		for (int j = 0; j < donjon.getX(); j++) {
			System.out.print("+---");
		}
		System.out.println("+");
	}
	
	// Affichage de la map avec le joueur Et les Monstres, Et les objets
		public void displayDonjonGamerItemsMonster() {
			for (int i = 0; i < donjon.getY(); i++) {
				// draw the north edge
				for (int j = 0; j < donjon.getX(); j++) {
					System.out.print(donjon.getDonjon()[j][i].getDoorN());
				}
				System.out.println("+");
				// draw the west edge
				for (int j = 0; j < donjon.getX(); j++) {
					if(j == 0 && i == 0) {
						System.out.print(donjon.getDonjon()[j][i].getDoorW() );
					} else if(donjon.getDonjon()[j][i].getId() == donjon.getEnd()){
						if((donjon.getDonjon()[j][i].getBit() & 8) == 0) {
							System.out.print("|>< " );
						} else {
							System.out.print(" >< " );
						}
					}else {
							System.out.print(donjon.getDonjon()[j][i].getDoorW());	
					}
				}
				System.out.println("|");
			}
			// draw the bottom line
			for (int j = 0; j < donjon.getX(); j++) {
				System.out.print("+---");
			}
			System.out.println("+");
		}
	

	
//-------------------------------------------------------------MOTEUR DU JEU----------------------------------------------------------------------------------------------------
	public void letsPlay() {
		int x = 0; int y = 0;
		for( int i = 0; i < donjon.getX(); i++) {
			for(int j = 0; j < donjon.getY(); j++) {
				if(donjon.getDonjon()[i][j].getId() == donjon.getEnd()) {
					x = donjon.getDonjon()[i][j].getPositionX();
					y = donjon.getDonjon()[i][j].getPositionY();
					i = donjon.getX();
					j = donjon.getY();
					break;
				}
			}
		}
		System.out.println("les coordonnees de la deadRoom  x : "+x+", y :"+y);
		checkPossibility();
		setUserChoice();
		if (joueur.getPosX() != x || joueur.getPosY() != y) {
			letsPlay();
		} else {
			System.out.println("fini !");
		}
	}
	
	
	// Propose les diff�rentes possibilit�s d'avanc�es au joueur
	public void checkPossibility() {
		int x = joueur.getPosX(); int y = joueur.getPosY();
		
		String possibilities = "Vous pouvez vous d�placer";
		
		// down
		if(y + 1 < donjon.getY()  && donjon.getDonjon()[x][y+1].isdN() == false ) {
			possibilities += " en Bas";
		} 
		// right
		if(x + 1 < donjon.getX()  && donjon.getDonjon()[x+1][y].isdW() == false ) {
			possibilities += " a Droite";
		} 
		// up
		if( y -1 >= 0  && donjon.getDonjon()[x][y-1].isdS() == false ) {
			possibilities += " en Haut";
		} 
		// left
		if(x -1 >= 0 && donjon.getDonjon()[x-1][y].isdE() == false ) {
			possibilities += " a Gauche";
		} 
		
		System.out.println(possibilities
						  +"\nQue Choisissez-vous ?"
						  + "Pressez la touche Q pour aller � gauche, Z pour monter, D pour tourner � droite et S pour descendre");
	}
	
	// Methodes de v�rification du d�placement
	public boolean checkDown() {
		if(joueur.getPosY() + 1 < donjon.getY()  && donjon.getDonjon()[joueur.getPosX()][joueur.getPosY()+1].isdN() == false ) {
			return true;
		} 
		return false;
	}
	public boolean checkUp() {
		if( joueur.getPosY() -1 >= 0  && donjon.getDonjon()[joueur.getPosX()][joueur.getPosY()-1].isdS() == false ) {
			
			return true;
		} 
		return false;
	}
	public boolean checkRight() {
		if(joueur.getPosX() + 1 < donjon.getX()  && donjon.getDonjon()[joueur.getPosX()+1][joueur.getPosY()].isdW() == false ) {
			return true;
		} 
		return false;
	}	
	public boolean checkLeft() {
		if(joueur.getPosX() -1 >= 0 && donjon.getDonjon()[joueur.getPosX()-1][joueur.getPosY()].isdE() == false ){
			return true;
		} 
		return false;
	}
	
	//D�place le joueur en fonction de la r�ponse de l'utilisateur
	public void setUserChoice(){
		Scanner in = new Scanner(System.in);
		String choice = in.next();
		if (choice.equalsIgnoreCase("Q")) {
			if(checkLeft()) {
				System.out.println("Allons dans la salle de gauche !");
				GamerDisplacement("x", -1 );
			} else {
				System.out.println("Vous �tes un petit t�m�raire mais vous vous prenez un mur en pleine figure. Recommencez et vous perdez un point de vie. Merci de refaire votre saisie");
				setUserChoice();
			}
		} else if (choice.equalsIgnoreCase("Z")) {
			if(checkUp()) {
				System.out.println("Allons dans la salle d'en haut !");
				GamerDisplacement("y", -1 );
			} else {
				System.out.println("Vous �tes un petit t�m�raire mais vous vous prenez un mur en pleine figure. Recommencez et vous perdez un point de vie. Merci de refaire votre saisie");
				setUserChoice();
			}
		} else if (choice.equalsIgnoreCase("D")) {
			if(checkRight()) {
				System.out.println("Allons dans la salle de droite !");
				GamerDisplacement("x", 1 );
			} else {
				System.out.println("Vous �tes un petit t�m�raire mais vous vous prenez un mur en pleine figure. Recommencez et vous perdez un point de vie. Merci de refaire votre saisie");
				setUserChoice();
			}
		} else if (choice.equalsIgnoreCase("S")) {
			if(checkDown()) {
				System.out.println("Allons dans la salle du bas !");
				GamerDisplacement("y", 1 );
			} else {
				System.out.println("Vous �tes un petit t�m�raire mais vous vous prenez un mur en pleine figure. Recommencez et vous perdez un point de vie. Merci de refaire votre saisie");
				setUserChoice();
			}
		} else {
			System.out.println("Merci de taper la bonne direction");
			setUserChoice();
		}
	}
	
	
	//Methode servant a d�placer le joueur dans la bonne salle
	public void GamerDisplacement(String coord, int direction ) {
		// suppression de la position du joueur dans la map
		for (int i = 0; i < donjon.getX(); i++) {
			for(int j = 0; j < donjon.getY(); j++) {
				if((donjon.getDonjon()[i][j].getPositionY() == joueur.getPosY()) && (donjon.getDonjon()[i][j].getPositionX() == joueur.getPosX())){
					if(donjon.getDonjon()[i][j].isdW() == true) {
						donjon.getDonjon()[i][j].setDoorW("|   ");
					} else {
						donjon.getDonjon()[i][j].setDoorW("    ");
					}
					break;
				}
			}
		}
		System.out.println(coord+" "+direction);
		// D�placement du joueur dans la map et chamgement de ses coordonn�es
		if ("x".equals(coord)) {
			// A droite
			if(direction == 1) {
				for (int i = 0; i < donjon.getX(); i++) {
					for(int j = 0; j < donjon.getY(); j++) {
						if((donjon.getDonjon()[i][j].getPositionY() == joueur.getPosY()) && (donjon.getDonjon()[i][j].getPositionX() == joueur.getPosX()+1)){
							System.out.println("Je vais � droite!");
							if(donjon.getDonjon()[i][j].isdW() == true) {
								donjon.getDonjon()[i][j].setDoorW("| J ");
							} else {
								donjon.getDonjon()[i][j].setDoorW("  J ");
							}
							joueur.setPosX(joueur.getPosX()+1);
							j = donjon.getY();
							i = donjon.getX();
							break;
						}
						
					}
					
				}
			// A gauche
			}else if(direction == -1) {
				for (int i = 0; i < donjon.getX(); i++) {
					for(int j = 0; j < donjon.getY(); j++) {
						if((donjon.getDonjon()[i][j].getPositionY() == joueur.getPosY()) && (donjon.getDonjon()[i][j].getPositionX() == joueur.getPosX()-1)){
							if(donjon.getDonjon()[i][j].isdW() == true) {
								donjon.getDonjon()[i][j].setDoorW("| J ");
							} else {
								donjon.getDonjon()[i][j].setDoorW("  J ");
							}
							joueur.setPosX(joueur.getPosX()-1);
							j = donjon.getY();
							i = donjon.getX();
							break;
						}
					}
				}
			}
		} else if("y".equals(coord)) {
			// en bas
			if(direction == 1) {
				for (int i = 0; i < donjon.getX(); i++) {
					for(int j = 0; j < donjon.getY(); j++) {
						if((donjon.getDonjon()[i][j].getPositionX() == joueur.getPosX()) && (donjon.getDonjon()[i][j].getPositionY() == joueur.getPosY()+1)){
							if(donjon.getDonjon()[i][j].isdW() == true) {
								donjon.getDonjon()[i][j].setDoorW("| J ");
							} else {
								donjon.getDonjon()[i][j].setDoorW("  J ");
							}
							joueur.setPosY(joueur.getPosY()+1);
							j = donjon.getY();
							i = donjon.getX();
							break;
						}
					}
				}
			// en haut
			}else if(direction == -1) {
				for (int i = 0; i < donjon.getX(); i++) {
					for(int j = 0; j < donjon.getY(); j++) {
						if((donjon.getDonjon()[i][j].getPositionX() == joueur.getPosX()) && (donjon.getDonjon()[i][j].getPositionY() == joueur.getPosY()-1)){
							if(donjon.getDonjon()[i][j].isdW() == true) {
								donjon.getDonjon()[i][j].setDoorW("| J ");
							} else {
								donjon.getDonjon()[i][j].setDoorW("  J ");
							}

							joueur.setPosY(joueur.getPosY()-1);
							j = donjon.getY();
							i = donjon.getX();
							break;
						}
					}
				}
			}
		}
		joueur.getPositions();
		displayDonjonGamer();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Jeu jeu = new Jeu();
		System.out.println("Bonjour ! Bienvenue dans le donjon ! Houuu ca fait peur....");
		jeu.donjon.getEnd();
		jeu.displayDonjonGamerItemsMonster();
		jeu.displayDonjonGamer();
		System.out.println("Vous �tes � l'entr�e du Donjon.");
		
		jeu.letsPlay();


	}

}
