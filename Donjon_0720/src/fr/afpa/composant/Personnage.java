package fr.afpa.composant;

public abstract class Personnage {
	
	private String nom;
	private int pointDeVie;
	private int pointDeForce;
	private int or;
	private int posX;
	private int posY;

	
	public Personnage(String nom, int pointDeVie, int pointDeForce, int or) {
		this.nom = nom;
		this.pointDeVie = pointDeVie;
		this.pointDeForce = pointDeForce;
		this.or = or;
		
	}
	
	public Personnage() {}
	
	//abstract void attaquer();
}
