package fr.afpa.composant;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.afpa.plateau.Donjon;

public class ItemsMonsterCheptel {
	List <Monstre>monsterCheptel;
	List <ItemGame>itemsCheptel;
	int nbMonstre;
	int nbItem;
	
	
	public ItemsMonsterCheptel (int nbItem, int nbMonstre, int x, int y) {
		this.nbMonstre = nbMonstre;
		this.nbItem = nbItem;
		monsterCheptel = new ArrayList();
		itemsCheptel = new ArrayList();
	//	 On int�gre le nombre voulu d'item dans la list itemsCheptel
		for (int i = 0; i < nbItem/4; i++) {
			itemsCheptel.add(new BourseOr(randInt(1, x-1), randInt(1, y-1)));
			itemsCheptel.add(new BanditManchot(randInt(1, x-1), randInt(1, y-1)));
			itemsCheptel.add(new PotionForce(randInt(1, x-1), randInt(1, y-1)));
			itemsCheptel.add(new PotionVie(randInt(1, x-1), randInt(1, y-1)));
		}
		
		for (int i = 0; i < this.nbMonstre; i++ ) {
			monsterCheptel.add(new Monstre(randInt(1, x-1), randInt(1, y-1)));
		}
	}
	
	
	

	public List<Monstre> getMonsterCheptel() {
		return monsterCheptel;
	}
	public void setMonsterCheptel(List<Monstre> monsterCheptel) {
		this.monsterCheptel = monsterCheptel;
	}

	public List<ItemGame> getItemsCheptel() {
		return itemsCheptel;
	}
	public void setItemsCheptel(List<ItemGame> itemsCheptel) {
		this.itemsCheptel = itemsCheptel;
	}




	public static int randInt(int min, int max) {
	    Random Rand = new Random();
	    int randomNum = Rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
}
