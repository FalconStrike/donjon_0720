package fr.afpa.composant;

public class Joueur extends Personnage {
	
	private int posX;
	private int posY;

	public Joueur (String nom, int pointDeVie, int pointDeForce, int or) {
		super(nom, pointDeVie, pointDeForce, or);
	}
	
	public Joueur (int posX, int posY) {
		super("jojo", 40, 10, 10);
		this.posX = posX;
		this.posY = posY;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	
	public void getPositions() {
		System.out.println("x : "+posX+", y :"+posY);
	}
}
