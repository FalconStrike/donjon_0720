package fr.afpa.plateau;

public class Salle {
	
	private int id;
	private int bit;
	private int positionX;
	private int positionY;
	private boolean isVisited;
	private String doorN;
	private boolean dN;
	private String doorW;
	private boolean dW;
	private String doorS;
	private boolean dS;
	private String doorE;
	private boolean dE;
	
	
	public static int count =10;
	
	public Salle(int bit, int positionX, int positionY) {
		this.id = ++count;
		this.bit = bit;
		this.positionX = positionX;
		this.positionY = positionY;
	}
	
	public Salle(int bit, int positionX, int positionY, String doorW) {
		this.id = ++count;
		this.bit = bit;
		this.positionX = positionX;
		this.positionY = positionY;
		this.doorW = doorW;
	}
	
	public Salle(int id, int bit, int positionX, int positionY, String doorW, String doorN, boolean dW, boolean dN) {
		this.id = id;
		this.bit = bit;
		this.positionX = positionX;
		this.positionY = positionY;
		this.doorW = doorW;
		this.doorN = doorN;
		this.dW = dW;
		this.dN = dN;
	}
	
	
	@Override
	public String toString() {
		return "Salle [id=" + id + ", bit=" + bit + ", positionX=" + positionX + ", positionY=" + positionY
				+ ", isVisited=" + isVisited + ", doorN=" + doorN + ", dN=" + dN + ", doorW=" + doorW + ", dW=" + dW
				+ "]";
	}



	public boolean isdN() {
		return dN;
	}
	public void setdN(boolean dN) {
		this.dN = dN;
	}
	public boolean isdW() {
		return dW;
	}
	public void setdW(boolean dW) {
		this.dW = dW;
	}



	public String getDoorN() {
		return doorN;
	}
	public void setDoorN(String doorN) {
		this.doorN = doorN;
	}
	public String getDoorW() {
		return doorW;
	}
	public void setDoorW(String doorW) {
		this.doorW = doorW;
	}

	public boolean isdS() {
		return dS;
	}

	public void setdS(boolean dS) {
		this.dS = dS;
	}

	public String getDoorS() {
		return doorS;
	}

	public void setDoorS(String doorS) {
		this.doorS = doorS;
	}

	public String getDoorE() {
		return doorE;
	}

	public void setDoorE(String doorE) {
		this.doorE = doorE;
	}

	public boolean isdE() {
		return dE;
	}

	public void setdE(boolean dE) {
		this.dE = dE;
	}

	public int getId() {
		return id;
	}
	public int getBit() {
		return bit;
	}
	public void setBit(int bit) {
		this.bit = bit;
	}
	public int getPositionX() {
		return positionX;
	}
	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}
	public int getPositionY() {
		return positionY;
	}
	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
	public boolean isVisited() {
		return isVisited;
	}
	public void setVisited(boolean isVisited) {
		this.isVisited = isVisited;
	}
	
	
	public static int getCount() {
		return count;
	}
	public static void setCount(int count) {
		Salle.count = count;
	}



	public void getPositions() {
		System.out.println("x :"+positionX+", y :"+positionY);
	}
}
