package fr.afpa.plateau;

import fr.afpa.composant.Joueur;

public class AffichageDonjon {
	private Donjon donjon;
	// Affichage donjon avec coordonn�es de room
	public void displayDonjonGamer() {
		for (int i = 0; i < donjon.getY(); i++) {
			// draw the north edge
			
			for (int j = 0; j < donjon.getX(); j++) {
				System.out.print(donjon.getDonjon()[j][i].getDoorN());
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < donjon.getX(); j++) {
				if(j == 0 && i == 0) {
					System.out.print((donjon.getDonjon()[j][i].getBit() & 1) == 0 ? ">   " : "+   ");
				} else if(donjon.getDonjon()[j][i].getId() == donjon.getEnd()){
					if((donjon.getDonjon()[j][i].getBit() & 8) == 0) {
						System.out.print("|>< " );
					} else {
						System.out.print(" >< " );
					}
					
				}else {
					System.out.print(donjon.getDonjon()[j][i].getDoorW());
	
				}
			}
			System.out.println("|");
		}
		// draw the bottom line
		for (int j = 0; j < donjon.getX(); j++) {
			System.out.print("+---");
		}
		System.out.println("+");
		
	}
	
	 public static void main (String [] args) {
		 
		 // D�but du jeu 
		 System.out.println("Bonjour ! Bienvenue dans le donjon ! Houuu ca fait peur....");
		 
		 Joueur jojo = new Joueur(0,0);
		 Donjon donjon = new Donjon(6, 3);

//		 donjon.generateMaze(0, 0);
//		 donjon.generateAndCheck(0,0);
//		 donjon.display();
//		 displayDonjonGamer();
	 }
	 
	
}
